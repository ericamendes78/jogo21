
public enum Nipe {
	ESPADA("Espada"),
	COPAS("Copas"),
	PAUS("Paus"),
	OURO("Ouro");
	
	private String descricao;

	private Nipe(String novoDescricao) {
		descricao = novoDescricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
