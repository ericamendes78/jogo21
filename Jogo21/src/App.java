import java.util.Scanner;

public class App {

	public static void main(String[] args) {	
		
		//Inicializa o baralho
		Baralho baralho = new Baralho();
		baralho.listarBaralho(2);
	
		//Informa o jogador 1
		Jogador jogador1 = new Jogador("Joao");
		jogador1.calcularPontos(baralho);
		
		System.out.println("\n" + jogador1.toString());
		
		boolean parouJogo = false;
		
		while(!parouJogo) {
			
			Scanner s = new Scanner(System.in);
			
			System.out.println("\nO que deseja fazer? (J) - Jogar/Tirar nova carta ou (P) - Parar");
			String valor = s.nextLine();
			
			if(valor.equals("J")) {
				baralho.listarBaralho(1);
				jogador1.calcularPontos(baralho);

			}else {
				jogador1.calcularPontos(baralho);
				parouJogo = true;
			}
			
			if(jogador1.getPontos() > 21 ) {
				System.out.println("Voce perdeu o jogo. Tente outro dia :(");
				parouJogo = true;
			}else if(jogador1.getPontos() == 21 ) {
				System.out.println("Voce ganhou o jogo.");
				parouJogo = true;
			}
			
			System.out.println("\n" + jogador1.toString());
			
		}
	}

}
