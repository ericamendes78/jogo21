import java.util.ArrayList;

public class Carta {

	private String campo;
	private int valor;
	private Nipe nipe;

	private String getCampo() {
		return campo;
	}

	private void setCampo(String campo) {
		this.campo = campo;
	}

	public int getValor() {
		return valor;
	}

	private void setValor(int valor) {
		this.valor = valor;
	}

	public Nipe getNipe() {
		return nipe;
	}

	private void setNipe(Nipe nipe) {
		this.nipe = nipe;
	}

	public ArrayList<Carta> gerarListaCartas() {

		String[] listaCarta = { "As", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" };
		ArrayList<Carta> cartas = new ArrayList<Carta>();

		for (int i = 0; i < listaCarta.length; i++) {

			for(Nipe n : Nipe.values()) {
				Carta carta = new Carta();
				carta.setCampo(listaCarta[i]);
				if (carta.getCampo() == "J" || carta.getCampo() == "K" || carta.getCampo() == "Q") {
					carta.setValor(10);
				}else if(carta.getCampo() == "As") {
					carta.setValor(1);
				}else {
					carta.setValor(Integer.parseInt(listaCarta[i]));
				}
				carta.setNipe(n);
				cartas.add(carta);
			}
		}

		return cartas;

	}
	
	
	public String toString() {
		return getCampo() +  " - " + getNipe() ;
	}
	

}
