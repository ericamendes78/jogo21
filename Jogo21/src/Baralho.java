import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Baralho {

	private List<Carta> baralho;
	private List<Carta> cartasSorteada;

	public Baralho() {
		inicializarBaralho();
		cartasSorteada = new ArrayList<Carta>();
	}

	public void inicializarBaralho() {
		Carta c = new Carta();
		baralho = c.gerarListaCartas();
	}

	public List<Carta> getBaralho() {
		return baralho;
	}

	public void setBaralho(ArrayList<Carta> baralho) {
		this.baralho = baralho;
	}
	
	public List<Carta> getCartasSorteada() {
		return cartasSorteada;
	}

	public void setCartasSorteada(ArrayList<Carta> cartaSorteada) {
		this.cartasSorteada = cartaSorteada;
	}

	public Carta gerarBaralhoRandom() {
		int index = (int) Math.ceil(Math.random() * 51);
		return baralho.get(index);
	}

	public void listarBaralho(int quantidadeCartas) {
		Carta c = null;
		for (int i = 0; i < quantidadeCartas; i++) {
			c = gerarBaralhoRandom();
			cartasSorteada.add(c);
			System.out.println(c);
		}
		
	}

}
