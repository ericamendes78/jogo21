
public class Jogador {
	
	private String nome;
	private int pontos;
	
	public Jogador(String nome){
		setNome(nome);
		setPontos(0);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getPontos() {
		return pontos;
	}

	public void setPontos(int pontos) {
		this.pontos = pontos;
	}
	
	public void calcularPontos(Baralho baralho) {
		int somaPontos = 0;
		for (Carta c : baralho.getCartasSorteada()) {
			somaPontos += c.getValor();
		}
		setPontos(somaPontos);
	}
	
	public String toString() {
		return "Nome: " +  getNome() + "\nPontos:" + getPontos(); 
	}

}
